import { browser, element, by } from 'protractor';

describe('Login Form', () => {
    beforeEach(() => {
        browser.get('/login');
    });

    it('should display login form', () => {
        expect(element(by.css('form')).isDisplayed()).toBeTruthy();
    });

    it('should disable submit button if form is invalid', () => {
        element(by.css('input[name=username]')).sendKeys('');
        element(by.css('input[name=password]')).sendKeys('');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBeFalsy();
    });

    it('should have a username textbox', () => {
        expect(element(by.css('input[name=username]')).isDisplayed()).toBeTruthy();
    });

    it('should have a password textbox', () => {
        expect(element(by.css('input[name=password]')).isDisplayed()).toBeTruthy();
    });
});
