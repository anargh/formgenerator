import { FormArray } from '@angular/forms';

export class CheckboxRequired {
  static checkRequired(formArray: FormArray) {
    let valid = false;
    for (let index = 0; index < formArray.length; ++index) {
        if (formArray.at(index).value) {
            valid = true;
            break;
        }
    }
    return valid ? null : {
        'required': true
    };
  }
}
