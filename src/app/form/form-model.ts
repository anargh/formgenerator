import { ValidatorFn, AbstractControl } from '@angular/forms';
import { ErrorHandler, Injectable } from '@angular/core';

// export class FormModel<Type> {
//     formID: number;
//     formName: string;
//     formControls: {
//         label?: string;
//         key?: string;
//         value?: Type;
//         placeholder?: string;
//         type?: string;
//         validations?: FormValidationModel;
//         rows?: number;
//         cols?: number;
//         options?: {
//             key?: string;
//             value?: string;
//             label?: string;
//             default?: boolean;
//         }[];

//         /////////////////////////////
//         // Values for Control Type: textbox, dropdown, selectbox, textareabox
//         ////////////////////////////
//         controlType: string;
//     }[];
//     submissionControls?: {
//         submit: {
//             key: string;
//             label: string;
//         };
//         reset?: {
//             key: string;
//             label: string;
//         }
//     };

//     // TODO: If value is not given, it is undefined. Give a default value here if its undefined.
//     constructor() {    }
// }

const REQUIRED_FIELDS = ['formID', 'formName', 'formControls', ''];

export class FormModel {
    formID: number;
    formName: string;
    formControls: Array<TextBox | DropDown | TextAreaBox | SelectBox>;
    submissionControls?: {
        submit: {
            key: string;
            label: string;
            validityCheck?: boolean;
        };
        reset?: {
            key: string;
            label: string;
        }
    };
}


export interface FormValidationModel {
    required?: boolean;
    minlength?: number;
    maxlength?: number;
    pattern?: string;
    customValidations?: CustomFormValidationModel[];
}

export interface NumberBoxValidationModel extends FormValidationModel {
    min?: number;
    max?: number;
}

export class CustomFormValidationModel {
    validator: ValidatorFn | AbstractControl;
    message: string;
}

export class DynamicFormControl {
    label: string;
    key: string;
    controlType: string;
}

export class TextBox extends DynamicFormControl {
    value?: string;
    placeholder?: string;
    type: string;
    validations?: FormValidationModel;
    step?: number;                                  // step attribute for input type = number
    accept?: string;                                // accept attribute for input type = file
    multiple?: boolean;                             // multiple attribute for input type = file
}

export class NumberBox extends DynamicFormControl {
    value?: number;
    placeholder?: string;
    type: string;
    validations?: NumberBoxValidationModel;
}

export class TextAreaBox extends DynamicFormControl {
    value?: string;
    placeholder?: string;
    rows?: number;
    cols?: number;
}

export class DropDown extends DynamicFormControl {
    validations?: FormValidationModel;
    multiple?: boolean;
    options?: {
        label: string;
        value: string;
        default?: boolean;
    }[];
}

export class SelectBox extends DynamicFormControl {
    type?: string;
    validations?: FormValidationModel;
    options?: {
        key: string;
        value: string;
        label?: string;
        default?: boolean;
    }[];
}

@Injectable()
export class FormError extends ErrorHandler {

    public errorMessage;

    handleError(error: any): void {
        this.errorMessage = error.message;
    }

    hasError(): any {
        if (this.errorMessage) {
            return true;
        } else {
            return false;
        }
    }
}
