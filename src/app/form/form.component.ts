import { Component, OnInit, Input, HostListener } from '@angular/core';
import { FormModel, FormValidationModel, DynamicFormControl } from './form-model';
import { FormGroup, FormControl, Validators, FormArray, CheckboxRequiredValidator } from '@angular/forms';
import { CheckboxRequired } from './validators/checkbox-required.validator';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

    @Input() formJSON: string;
    private formData: FormModel;
    private dynamicFormGroup: FormGroup;
    private formErrorMessage: string;
    private isFormValid: boolean;

    // File Input Element properties
    private acceptedTypes: string;
    private fileDragAreaClass: string;
    private fileErrors = [];
    private maxFiles = 3;

    constructor() { }

    ngOnInit() {
        this.isFormValid = true;
        try {
            this.formData = JSON.parse(this.formJSON);
            this.initForm();
        } catch (error) {
            this.isFormValid = false;
            this.formErrorMessage = error.message;
            console.log(error);
        }
        // this.formData = JSON.parse(this.formJSON);
        // this.initForm();
    }

    // Intialize form
    initForm() {
        this.dynamicFormGroup = this.convertToFormGroup(this.formData);
        this.onChanges();
        console.log(this.dynamicFormGroup.value);
    }

    // Convert the JSON to Form Group
    convertToFormGroup(form: FormModel) {
        const group = {};
        let formValidations = [];
        form.formControls.forEach(control => {
            if (control.controlType === 'selectbox') {
                if (control['type'] === 'checkbox') {
                    group[control.key] = this.getCheckboxGroup(control);
                }
                if (control['type'] === 'radio') {
                    group[control.key] = this.getRadioGroup(control);
                }
            } else {
                formValidations = this.getFormValidations(control['validations'], control['type']);
                group[control.key] = new FormControl(control['value'], formValidations);
            }
        });
        return new FormGroup(group);
    }

    getCheckboxGroup(control: DynamicFormControl) {
        let checkboxArray = new FormArray([]);
        if (control['validations'].required) {
            checkboxArray = new FormArray([], CheckboxRequired.checkRequired);
        }
        control['options'].forEach(result => {
            checkboxArray.push(new FormControl(null));
        });
        return checkboxArray;
    }

    getRadioGroup(control) {
        const formValidations = this.getFormValidations(control['validations'], control['type']);
        const radioGroup = new FormGroup({});
        control['options'].forEach(result => {
            radioGroup.addControl(control.key, new FormControl(null, formValidations));
        });
        return radioGroup;
    }

    // Returns the validations to be done for a single form control
    getFormValidations(validationOptions: FormValidationModel, formControlType: string) {
        const validations = [];
        if (validationOptions === undefined) {
            return validations;
        }
        if (validationOptions.required && formControlType !== 'checkbox') {
            validations.push(Validators.required);
        }
        // if (validationOptions.required && formControlType === 'checkbox') {
        //     validations.push(Validators.requiredTrue);
        // }
        if (validationOptions.pattern) {
            validations.push(Validators.pattern(validationOptions['pattern']));
        }
        if (validationOptions['minlength']) {
            validations.push(Validators.minLength(validationOptions['minlength']));
        }
        if (validationOptions['maxlength']) {
            validations.push(Validators.maxLength(validationOptions['maxlength']));
        }
        if (validationOptions['min']) {
            validations.push(Validators.min(validationOptions['min']));
        }
        if (validationOptions['max']) {
            validations.push(Validators.max(validationOptions['max']));
        }
        if (validationOptions.customValidations) {
            validationOptions.customValidations.forEach(validation => {
                validations.push(validation);
            });
        }
        return validations;
    }

    // INPUT TYPE FILE Methods

    onFileChange(event) {
        this.isFilesValid(event.target.files);
    }

    // @HostListener('dragover', ['$event']) fileInputDragOver(event) {
    //     this.fileDragAreaClass = 'dropzone';
    //     console.log('Drag Over');
    //     event.preventDefault();
    // }

    // @HostListener('dragenter', ['$event']) fileInputDragEnter(event) {
    //     this.fileDragAreaClass = 'dropzone';
    //     console.log('Drag Enter');
    //     event.preventDefault();
    // }

    // @HostListener('dragend', ['$event']) fileInputDragEnd(event) {
    //     this.fileDragAreaClass = 'dragarea';
    //     console.log('Drag End');
    //     event.preventDefault();
    // }

    // @HostListener('dragleave', ['$event']) fileInputDragLeave(event) {
    //     this.fileDragAreaClass = 'dragarea';
    //     console.log('Drag Leave');
    //     event.preventDefault();
    // }

    // When file/files are dropped.
    @HostListener('drop', ['$event']) fileInputDrop(event) {

        this.fileDragAreaClass = 'dragarea';
        // Allow the drop by preventDefault()
        event.preventDefault();
        event.stopPropagation();

        // Get the files
        const files = event.dataTransfer.files;

        // Get the control name
        const controlName = event.target.attributes['ng-reflect-name'].value;
        event.target.files = files;

        // Get the accepted file extensions from the form controls attribute and store it.
        this.acceptedTypes = event.target.accept ? event.target.accept.split(',').map(format => format.trim()) : '';

        // Mark the form control as dirty and touched
        this.dynamicFormGroup.get(controlName).markAsDirty();
        this.dynamicFormGroup.get(controlName).markAsTouched();

        // Check if the files are valid
        if (!this.isFilesValid(files)) {
            this.dynamicFormGroup.get(controlName).setErrors({
                fileErrors: true
            });
        } else {
            this.dynamicFormGroup.get(controlName).setErrors(null);
            this.dynamicFormGroup.get(controlName).updateValueAndValidity();
        }
    }

    isFilesValid(files: any) {
        this.fileErrors = [];
        if (files.length > this.maxFiles) {
            this.fileErrors.push(`ERROR: Maximum of ${this.maxFiles} can be uploaded.`);
            return false;
        }

        if (!this.validateFileFormat(files)) {
            return false;
        }

        return true;
    }

    validateFileFormat(files): boolean {
        let fileFormat = '';
        let valid: boolean;
        for (let index = 0; index < files.length; index++) {
            fileFormat = files[index].name.substring(files[index].name.lastIndexOf('.'), files[index].name.length)
                            || files[index].name;
            if (!this.acceptedTypes.includes(fileFormat)) {
                valid = false;
                this.fileErrors.push(`ERROR: File Format of ${files[index].name} is not acceptable. 
                                        Acceptable formats are ${this.acceptedTypes}`);
            } else {
                valid = true;
            }
        }
        return valid;
    }
    // TODO: REMOVE! onChanges() - FOR DEBUGGING
    onChanges(): void {
        this.dynamicFormGroup.valueChanges.subscribe(val => {
            console.log(this.dynamicFormGroup);
            console.log(this.dynamicFormGroup.value);
        });
    }

}
