import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

    private loginFormJSON: string;

    constructor() { }

    ngOnInit() {
        this.loginFormJSON = `
        {
           "formID": 1,
           "formName": "Example Form",
           "submissionControls": {
                "submit" : {
                    "key": "submit",
                    "label": "Submit Label",
                    "validityCheck": true
                }
           },
           "formControls": [
            {
                "label": "TExt",
                "key": "Textbox",
                "controlType": "textbox",
                "type": "text",
                "validations": {
                   "required": true
                }
            },
            {
                "label": "TExt",
                "key": "filebox",
                "controlType": "textbox",
                "type": "file",
                "accept": ".png, .doc",
                "validations": {
                   "required": true
                }
            },
            {
                "label": "Checkbox",
                "key": "checkbox",
                "controlType": "selectbox",
                "type": "checkbox",
                "validations": {
                   "required": true
                },
                "options": [
                   {
                      "key": "one",
                      "value": "1",
                      "label": "One"
                   },
                   {
                      "key": "two",
                      "value": "2",
                      "label": "Two"
                   }
                ]
             },
             {
                "label": "Radiobox",
                "key": "radiobox",
                "controlType": "selectbox",
                "type": "radio",
                "validations": {
                   "required": true
                },
                "options": [
                   {
                      "key": "one1",
                      "value": "1",
                      "label": "One"
                   },
                   {
                      "key": "two2",
                      "value": "2",
                      "label": "Two"
                   }
                ]
             }
           ]
        }`;
    }

}
